provider "aws" {
  region     = "ap-southeast-1"
  access_key = "AKIA4J3QFQEYI2KY3NEI"
  secret_key = "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"
}
#authen AWS with credential

resource "aws_vpc" "proj-vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "proj-apache2"
  }
}
#create vpc

resource "aws_internet_gateway" "proj-gw" {
  vpc_id = aws_vpc.proj-vpc.id
}
#create internet gateway

resource "aws_route_table" "proj-route-table" {
  vpc_id = aws_vpc.proj-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.proj-gw.id
  }

  route {
    ipv6_cidr_block = "::/0"
    gateway_id      = aws_internet_gateway.proj-gw.id
  }

  tags = {
    Name = "proj-rt"
  }
}
#create custom route table

resource "aws_subnet" "subnet-1" {
  vpc_id            = aws_vpc.proj-vpc.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = "ap-southeast-1a"

  tags = {
    Name = "proj-subnet-1"
  }
}
#create subnet

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.subnet-1.id
  route_table_id = aws_route_table.proj-route-table.id
}
#associate subnet with route table

resource "aws_security_group" "allow_web_by_terra" {
  name        = "allow_web_traffic"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_vpc.proj-vpc.id

  ingress {
    description = "HTTPS"
    from_port   = 443
    to_port     = 447
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "sec_group_web"
  }
}
#create sec-group to aollow port 22,80,443

resource "aws_network_interface" "web_server_int" {
  subnet_id       = aws_subnet.subnet-1.id
  private_ips     = ["10.0.1.50"]
  security_groups = [aws_security_group.allow_web_by_terra.id]

  tags = {
    Name = "network_int_proj"
  }
}
#create a network interface with an ip in subnet that was create in step 4

resource "aws_eip" "one" {
  vpc                       = true
  network_interface         = aws_network_interface.web_server_int.id
  associate_with_private_ip = "10.0.1.50"
  depends_on                = [aws_internet_gateway.proj-gw]
}
#assign an elastic ip to the network interface created in step 7

output "server_public_ip" {
  value = aws_eip.one.public_ip
}

resource "aws_instance" "ByTerraform2" {
  ami               = "ami-055d15d9cfddf7bd3"
  instance_type     = "t2.micro"
  availability_zone = "ap-southeast-1a"
  key_name          = "terraform_keypair_2"

  network_interface {
    device_index         = 0
    network_interface_id = aws_network_interface.web_server_int.id
  }

  user_data = filebase64("apache2_setup.sh")

  tags = {
    Name = "apache2-web-by-terraform"
  }
}

#define network

# resource "aws_instance" "ByTerraform" {
#   ami           = "ami-055d15d9cfddf7bd3"
#   instance_type = "t2.micro"

#   tags = {
#     Name = "HelloTerraform"
# }
# }
#define resource
